Last DL Lab Deliverables
- Exercise1: Folder with the First Exercise Python Code (EX1_GradientDescent.py)
- Exercise2: Folder with the Second Exercise Python Code (EX2_SingleLayer.py)
- Exercise34: Folder with the Third and Fourth Exercise Python Code (EX34_multilayer.py)
- DL_4thLab_SofiaBReichl.pdf: Report with the final results.