import tensorflow as tf
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

def plot(results):
    
    for result in results:
        plt.plot(result[1])
    
    plt.legend([result[0] for result in results], loc='upper right')
    plt.ylabel('Loss')
    plt.xlabel('Iteration Number')
    plt.ylim(ymin=-1, ymax=25)
    #plt.savefig('EX1_Different_Steps.png')
    #plt.savefig('EX1_Different_Optimizers.png')
    plt.close()

# We will test 5 steps on the GradientDescentOptimizer
#v_optimizer = [['GradientDescent with 0.025', tf.train.GradientDescentOptimizer(0.025)],
#               ['GradientDescent with 0.01', tf.train.GradientDescentOptimizer(0.01)],
#               ['GradientDescent with 0.001', tf.train.GradientDescentOptimizer(0.001)],
#               ['GradientDescent with 0.0001', tf.train.GradientDescentOptimizer(0.0001)],
#               ['GradientDescent with 0.00001', tf.train.GradientDescentOptimizer(0.00001)]]

# We will also test 5 different Optimizers
v_optimizer = [['Gradient Descent', tf.train.GradientDescentOptimizer(0.01)],
               ['Adadelta', tf.train.AdadeltaOptimizer(0.01)],
               ['Adagrad', tf.train.AdagradOptimizer(0.01)],
               ['Adam', tf.train.AdamOptimizer(0.01)],
               ['RMSProp', tf.train.RMSPropOptimizer(0.01)]]
	
function = []

for opt in v_optimizer:

    optimizer_name = opt[0]
    
    print '##############################################'
    print ' Optimizer used: ' + optimizer_name
    print '##############################################'
	
	# Model parameters
    W = tf.Variable([.3], dtype=tf.float32)
    b = tf.Variable([-.3], dtype=tf.float32)
    # Model input and output
    x = tf.placeholder(tf.float32)
    linear_model = W * x + b
    y = tf.placeholder(tf.float32)
    
    # loss
    loss = tf.reduce_sum(tf.square(linear_model - y)) # sum of the squares
    # optimizer
    optimizer = opt[1]
    train = optimizer.minimize(loss)
    
    # training data
    x_train = [1, 2, 3, 4]
    y_train = [0, -1, -2, -3]
    # training loop
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init) # reset values to wrong
    
    curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
    print("W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))
    
    result = []
    
    for i in range(1000):
        sess.run(train, {x: x_train, y: y_train})
        curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
        #print("W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))
        result.append(curr_loss)
    
    function.append([optimizer_name, result])

print '\n\n FINAL RESULTS\n'

for result in function:
    print 'The algorithm', result[0], 'has a mean LOSS of:', np.mean(result[1]) ,'. And a lattest value of:', result[1][-1]

plot(function)
