#!/usr/bin/env python
import tensorflow as tf
import read_inputs
import numpy as N
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
#from tensorflow.examples.tutorials.mnist import input_data

def plot(results):
    for result in results:
        plt.plot(result[1])
    plt.legend([result[0] for result in results], loc='upper right')
    plt.ylabel('Loss')
    plt.xlabel('Iteration Number')
    #plt.savefig('EX2_Different_Steps.png')
    plt.savefig('EX2_Different_Optimizers.png')
    plt.close()

#read data from file
#data_input = input_data.read_data_sets("MNIST_data/mnist.pkl.gz")
data_input = read_inputs.load_data_mnist('MNIST_data/mnist.pkl.gz')
#FYI data = [(train_set_x, train_set_y), (valid_set_x, valid_set_y), (test_set_x, test_set_y)]
data = data_input[0]
#print ( N.shape(data[0][0])[0] )
#print ( N.shape(data[0][1])[0] )

#data layout changes since output should an array of 10 with probabilities
real_output = N.zeros( (N.shape(data[0][1])[0] , 10), dtype=N.float )
for i in range ( N.shape(data[0][1])[0] ):
  real_output[i][data[0][1][i]] = 1.0  

#data layout changes since output should an array of 10 with probabilities
real_check = N.zeros( (N.shape(data[2][1])[0] , 10), dtype=N.float )
for i in range ( N.shape(data[2][1])[0] ):
  real_check[i][data[2][1][i]] = 1.0


  
# We will test 5 steps on the GradientDescentOptimizers
#v_optimizer = [['GradientDescent with 0.9', tf.train.GradientDescentOptimizer(0.9)],
#               ['GradientDescent with 0.5', tf.train.GradientDescentOptimizer(0.5)],
#               ['GradientDescent with 0.01', tf.train.GradientDescentOptimizer(0.01)],
#               ['GradientDescent with 0.001', tf.train.GradientDescentOptimizer(0.001)],
#               ['GradientDescent with 0.0001', tf.train.GradientDescentOptimizer(0.0001)]]

# We will also test 5 different Optimizers
v_optimizer = [['Gradient Descent', tf.train.GradientDescentOptimizer(0.01)],
               ['Adadelta', tf.train.AdadeltaOptimizer(0.01)],
               ['Adagrad', tf.train.AdagradOptimizer(0.01)],
               ['Adam', tf.train.AdamOptimizer(0.01)],
               ['RMSProp', tf.train.RMSPropOptimizer(0.01)]]
	
function = []

for opt in v_optimizer:

    optimizer_name = opt[0]
    
    print '##############################################'
    print ' Optimizer used: ' + optimizer_name
    print '##############################################'

    # Set up the computation. Definition of the variables.
    x = tf.placeholder(tf.float32, [None, 784])
    W = tf.Variable(tf.zeros([784, 10]))
    b = tf.Variable(tf.zeros([10]))
    y = tf.nn.softmax(tf.matmul(x, W) + b)
    y_ = tf.placeholder(tf.float32, [None, 10])
    
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
    
    train_step = opt[1].minimize(cross_entropy)
    
    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()
    
    # TRAINING PHASE
    print("TRAINING")

	result = []    
    for i in range(500):
      batch_xs = data[0][0][100*i:100*i+100]
      batch_ys = real_output[100*i:100*i+100]
      sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
	  
	  # We calculate the Loss and save it
      loss = sess.run(cross_entropy, feed_dict={x: batch_xs, y_: batch_ys}) 
      result.append(loss) 
 

    # CHECKING THE ERROR
    print("ERROR CHECK")
    
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    print(sess.run(accuracy, feed_dict={x: data[2][0], y_: real_check}))

print '\n\n FINAL RESULTS\n'

for result in function:
    print 'The algorithm', result[0], 'has a mean LOSS of:', N.mean(result[1]), '. And a lattest value of:', result[1][-1] 

plot(function)
