from __future__ import division
import keras
import numpy as np
print 'Using Keras version', keras.__version__
from keras.datasets import cifar10
from keras.constraints import maxnorm
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
#Load the cifar10 dataset, already provided by Keras
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

#Check sizes of dataset
print 'Number of train examples', x_train.shape[0]
#print 'Size of train examples', x_train.shape[1:]
print 'Number of test examples', x_test.shape[0]
#print 'Size of test examples', x_test.shape[1:]

#Normalize function with parameter data
def normalize(x):
	min_val = np.min(x)
	max_val = np.max(x)
	x = (x-min_val) / (max_val-min_val)
	return(x)
	

#Define number of classes:
num_classes = 10

#Adapt the data as an input of a fully-connected (flatten to 1D)
x_train = x_train.reshape(50000, 3, 32, 32).transpose(0, 2, 3, 1)
x_test = x_test.reshape(10000, 3, 32, 32).transpose(0, 2, 3, 1)

print 'NEW Size of train examples', x_train.shape[1:]

#Normalize data
x_train = normalize(x_train)
x_test = normalize(x_test)

#Adapt the labels to the one-hot vector syntax required by the softmax
from keras.utils.np_utils import to_categorical
y_train = to_categorical(y_train, num_classes)
y_test = to_categorical(y_test, num_classes)

#Define the NN architecture
from keras.models import Sequential
from keras.layers import Dense, Activation
##Two hidden layers
nn = Sequential()
nn.add(Conv2D(32,(3,3), activation='relu', kernel_constraint=maxnorm(3), padding='same',input_shape=(32,32,3)))
nn.add(Dropout(0.25))
nn.add(Conv2D(32,(3,3), activation='relu', kernel_constraint=maxnorm(3), padding='same'))
nn.add(MaxPooling2D(pool_size=(2,2)))

nn.add(Flatten())
nn.add(Dense(512, activation='relu'))
nn.add(Dropout(0.5))
nn.add(Dense(num_classes, activation='softmax'))


#Model visualization
#We can plot the model by using the ```plot_model``` function. We need to install *pydot, graphviz and pydot-ng*.
#from keras.utils import plot_model
#plot_model(nn, to_file='nn.png', show_shapes=true)

##Compile the NN
nn.compile(optimizer='sgd',loss='categorical_crossentropy',metrics=['accuracy'])

##Start training
history = nn.fit(x_train,y_train,batch_size=32,nb_epoch=25)

##Evaluate the model with test set
score = nn.evaluate(x_test, y_test, verbose=0)
print('test loss:', score[0])
print('test accuracy:', score[1])
#
###Store Plots
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
##Accuracy plot
#plt.plot(history.history['acc'])
#plt.title('model accuracy')
#plt.ylabel('accuracy')
#plt.xlabel('epoch')
#plt.legend(['train'], loc='upper left')
#plt.savefig('model_accuracy.pdf')
#plt.close()
##Loss plot
#plt.plot(history.history['loss'])
#plt.title('model loss')
#plt.ylabel('loss')
#plt.xlabel('epoch')
#plt.legend(['train'], loc='upper left')
#plt.savefig('model_loss.pdf')
#
##Confusion Matrix
#from sklearn.metrics import classification_report,confusion_matrix
#import numpy as np
##Compute probabilities
#Y_pred = nn.predict(x_test)
##Assign most probable label
#y_pred = np.argmax(Y_pred, axis=1)
##Plot statistics
#print 'Analysis of results' 
#target_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
#print(classification_report(np.argmax(y_test,axis=1), y_pred,target_names=target_names))
#print(confusion_matrix(np.argmax(y_test,axis=1), y_pred))
#
##Saving model and weights
#from keras.models import model_from_json
#nn_json = nn.to_json()
#with open('nn.json', 'w') as json_file:
#    json_file.write(nn_json)
#weights_file = "weights-MNIST_"+str(score[1])+".hdf5"
#nn.save_weights(weights_file,overwrite=True)
#
##Loading model and weights
#json_file = open('nn.json','r')
#nn_json = json_file.read()
#json_file.close()
#nn = model_from_json(nn_json)
#nn.load_weights(weights_file)
#
#