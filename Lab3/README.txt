I have made several experiments with the Alexa reviews dataset.
- Calculation of Sentiment
- Using several RNN
- Using different Optimization Methods
- Using different number of layers
- Using different epochs values
- Using different batch sizes

FILES:
00. Experiments
	(A) RNN
	(B) Optimization
	(C) Layers
	(D) Epochs
	(E) Batch Sizes
	(F) Others: Experiments with MySentiment variable
01. Codes 
	- Alexa.py
02. Data
	- amazon_alexa.tsv