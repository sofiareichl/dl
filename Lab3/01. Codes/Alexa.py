"""
.. module:: SentimentAlexaReviews

SentimentAlexaReviews
**********************

:Description: SentimentAlexaReviews
:Version: 1.0
:Created on: 07/12/2018 

"""

import pandas
from sklearn.metrics import confusion_matrix, classification_report
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import re
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding
from keras.layers import LSTM, GRU, SimpleRNN
from keras.optimizers import RMSprop, SGD, Adam, Adagrad
from keras.utils import np_utils
from collections import Counter
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import argparse
import time


def document_to_words(raw_doc):
    """
    Only keeps ascii characters in the tweet and discards @words

    :param raw_tweet:
    :return:
    """
    letters_only = re.sub("[^a-zA-Z@]", " ", raw_doc)
    words = letters_only.lower().split()
    ps = PorterStemmer()
    final_words = [ps.stem(word) for word in words if not word in set(stopwords.words('english'))]
    meaningful_words = [w for w in final_words if not re.match("^[@]", w)]
    return " ".join(meaningful_words)

	
def get_sentiment(reviews):
    sid = SentimentIntensityAnalyzer()
    #scores = reviews.apply(lambda x: sid.polarity_scores(x))  
    scores = []

	#Our dataframe consists of four columns from the sentiment scoring: Neu, Neg, Pos and compound. The first three represent the sentiment score percentage of each category in our headline, and the compound single number that scores the sentiment. `compound` ranges from -1 (Extremely Negative) to 1 (Extremely Positive).
	#We will consider posts with a compound value greater than 0.2 as positive and less than -0.2 as negative. There's some testing and experimentation that goes with choosing these ranges, and there is a trade-off to be made here. If you choose a higher value, you might get more compact results (less false positives and false negatives), but the size of the results will decrease significantly.
	#Let's create a positive label of 1 if the compound is greater than 0.2, and a label of -1 if compound is less than -0.2. Everything else will be 0.
	
    for line in reviews:
       pol_score = sid.polarity_scores(line)
       pol_score['headline'] = line
       scores.append(pol_score)


    sentiment_punct = [0 for k in scores]
    i = 0
    for score in scores:
        if score['compound'] > 0.2:
          sentiment_punct[i] = 1
        elif score['compound'] < -0.2:
          sentiment_punct[i] = -1
        i = i+1
    print(sentiment_punct[:3]) 
	
    return sentiment_punct
	
	
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', help="Verbose output (enables Keras verbose output)", action='store_true', default=False)
    parser.add_argument('--gpu', help="Use LSTM/GRU gpu implementation", action='store_true', default=False)
    args = parser.parse_args()

    verbose = 1 if args.verbose else 0
    impl = 2 if args.gpu else 0

    print("Starting:", time.ctime())

    ############################################
    # Data
	
    df_review = [line.rstrip() for line in open('amazon_alexa.tsv')]
    print("Dataset dimension:", len(df_review))
    
    df_review = pandas.read_csv('amazon_alexa.tsv', sep='\t')
    #print("Dataset head:")
    #df_review.head()
    
    # Pre-process the reviews and store in a separate column
    df_review['cleaned_review'] = df_review['verified_reviews'].apply(lambda x: document_to_words(x))
    df_review['length'] = df_review['verified_reviews'].apply(len)
    
	#print(df_review.groupby('rating').describe())
	
	# Calculate the sentiment
    df_review['sentiment_punct'] = get_sentiment(df_review['cleaned_review']) 
    print(df_review.groupby('sentiment_punct').describe())
	
    # Join all the words in review to build a corpus
    all_text = ' '.join(df_review['cleaned_review'])
    words = all_text.split()
    
    # Convert words to integers
    counts = Counter(words)
    
    numwords = 5000  # Limit the number of words to use
    vocab = sorted(counts, key=counts.get, reverse=True)[:numwords]
    vocab_to_int = {word: ii for ii, word in enumerate(vocab, 1)}
    
    review_ints = []
    for each in df_review['cleaned_review']:
        review_ints.append([vocab_to_int[word] for word in each.split() if word in vocab_to_int])
    
    # Create a list of labels
    labels = np.array(df_review['feedback'])
	#labels = np.array(df_review['sentiment_punct'])
    
    # Find the number of reviews with zero length after the data pre-processing
    review_len = Counter([len(x) for x in review_ints])
    print("Zero-length reviews: {}".format(review_len[0]))
    print("Maximum tweet length: {}".format(max(review_len)))
    
    # Remove those reviews with zero length and its corresponding label
    review_idx = [idx for idx, review in enumerate(review_ints) if len(review) > 0]
    labels = labels[review_idx]
    df_review = df_review.iloc[review_idx]
    review_ints = [review for review in review_ints if len(review) > 0]
    
    seq_len = max(review_len)
    features = np.zeros((len(review_ints), seq_len), dtype=int)
    for i, row in enumerate(review_ints):
        features[i, -len(row):] = np.array(row)[:seq_len]
    
    split_frac = 0.8
    split_idx = int(len(features) * 0.8)
    train_x, val_x = features[:split_idx], features[split_idx:]
    train_y, val_y = labels[:split_idx], labels[split_idx:]

    test_idx = int(len(val_x) * 0.5)
    val_x, test_x = val_x[:test_idx], val_x[test_idx:]
    val_y, test_y = val_y[:test_idx], val_y[test_idx:]

    print("\t\t\tFeature Shapes:")
    print("Train set: \t\t{}".format(train_x.shape),
          "\nValidation set: \t{}".format(val_x.shape),
          "\nTest set: \t\t{}".format(test_x.shape))

    print("Train set: \t\t{}".format(train_y.shape),
          "\nValidation set: \t{}".format(val_y.shape),
          "\nTest set: \t\t{}".format(test_y.shape))

    ############################################
    # Model
    drop = 0.0
    nlayers = 2  # >= 1
    #nlayers = 4  # >= 1
    #nlayers = 10  # >= 1
    RNN = LSTM
    #RNN = GRU
    #RNN = SimpleRNN

    neurons = 128
    embedding = 40

    model = Sequential()
    model.add(Embedding(numwords + 1, embedding, input_length=seq_len))

    if nlayers == 1:
        model.add(RNN(neurons, implementation=impl, recurrent_dropout=drop))
    else:
        model.add(RNN(neurons, implementation=impl, recurrent_dropout=drop, return_sequences=True))
        for i in range(1, nlayers - 1):
            model.add(RNN(neurons, recurrent_dropout=drop, implementation=impl, return_sequences=True))
        model.add(RNN(neurons, recurrent_dropout=drop, implementation=impl))

    model.add(Dense(2))
    model.add(Activation('softmax'))

    ############################################
    # Training

    learning_rate = 0.01
    #optimizer = SGD(lr=learning_rate, momentum=0.95)
    optimizer = Adagrad(lr=learning_rate, epsilon=None, decay=0.0)
    #optimizer = RMSprop(lr=learning_rate, rho=0.9, epsilon=None, decay=0.0)
    #optimizer = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

	#epochs = 20
    epochs = 50
	#batch_size = 25
	#batch_size = 50
    batch_size = 100

    train_y_c = np_utils.to_categorical(train_y, 2)
    val_y_c = np_utils.to_categorical(val_y, 2)

    history = model.fit(train_x, train_y_c,
                        batch_size=batch_size,
                        epochs=epochs,
                        validation_data=(val_x, val_y_c),
                        verbose=verbose)

    ############################################
    # Results

    test_y_c = np_utils.to_categorical(test_y, 2)
    score, acc = model.evaluate(test_x, test_y_c,
                                batch_size=batch_size,
                                verbose=verbose)
    
	
    ##Store Plots
    # Accuracy plot
    plt.plot(history.history['acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.savefig('model_accuracy_LSTM_Adagrad.pdf')
    plt.close()
    # Loss plot
    plt.plot(history.history['loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.savefig('model_loss_LSTM_Adagrad.pdf')

    print()
    print('Test ACC=', acc)

    test_pred = model.predict_classes(test_x, verbose=verbose)

    print()
    print('Confusion Matrix')
    print('-'*20)
    print(confusion_matrix(test_y, test_pred))
    print()
    print('Classification Report')
    print('-'*40)
    print(classification_report(test_y, test_pred))
    print()
    print("Ending:", time.ctime())