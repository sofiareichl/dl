import nltk
import gensim
import numpy as np 
import matplotlib

from gensim.models import Word2Vec
from nltk.cluster import KMeansClusterer  
from sklearn import cluster
from sklearn import metrics
from sklearn.decomposition import PCA
matplotlib.use('Agg')
from matplotlib import pyplot

  
def sent_vectorizer(sent, model):
    sent_vec =[]
    numw = 0
    for w in sent:
        try:
            if numw == 0:
                sent_vec = model[w]
            else:
                sent_vec = np.add(sent_vec, model[w])
            numw+=1
        except:
            pass
     
    return np.asarray(sent_vec) / numw
  
  
# training data

sentences = [["imagine","there","is","no","heaven"],
			["it","is","easy","if","you","try"],
			["no","hell","below","us"],
			["above","us","only","sky"],
			["imagine","all","the","people"],
			["living","for","today"],
			["imagine","there","is","no","countries"],
			["it","is","not","hard","to","do"],
			["nothing","to","kill","or","die","for"],
			["and","no","religion,","too"],
			["imagine","all","the","people"],
			["living","life","in","peace","you"],
			["you","may","say","I","am","a","dreamer"],
			["but","I","am","not","the","only","one"],
			["I","hope","someday","you","will","join","us"],
			["and","the","world","will","be","as","one"],
			["imagine","no","possessions"],
			["I","wonder","if","you","can"],
			["no","need","for","greed","or","hunger"],
			["a","brotherhood","of","man"],
			["imagine","all","the","people"],
			["sharing","all","the","world","you"],
			["you","may","say","I","am","a","dreamer"],
			["but","I","am","not","the","only","one"],
			["I","hope","someday","you","will","join","us"],
			["and","the","world","will","live","as","one"]]

			 
model = Word2Vec(sentences, min_count=1)

print 'Model similiarity sky and heaven: ', model.similarity('sky','heaven')
print 'Model similiarity live and die', model.similarity('live','die')
print 'Model similiarity kill and die', model.similarity('kill','die')
print 'Model similiarity imagine and dreamer', model.similarity('imagine','dreamer')

  
X=[]
for sentence in sentences:
    X.append(sent_vectorizer(sentence, model))   

#print ("========================")
#print 'X', X
 
print ("========================")
print 'M1', model[model.wv.vocab]
 
print ("========================")
#print 'M2', model.similarity('tonight', 'love')

print ("========================")
print 'M3', model.most_similar(positive=['imagine'], negative=[], topn=5)
print 'M3', model.most_similar(positive=[], negative=['imagine'], topn=5)

print ("========================")
print 'M4', model['the']
 
print "========================"
print 'M5', list(model.wv.vocab)

print "========================"
print 'M6', len(list(model.wv.vocab))
  
print "X shape", np.shape(X)  


# fit a 2d PCA model to the vectors
X2 = model[model.wv.vocab]
pca = PCA(n_components=2)
result = pca.fit_transform(X2)

# create a scatter plot of the projection
pyplot.scatter(result[:, 0], result[:, 1])
words = list(model.wv.vocab)
for i, word in enumerate(words):
	pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
pyplot.title('PCA')
#pyplot.savefig('PCA_Imagine.pdf')
pyplot.close()
	

NUM_CLUSTERS = 2
kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=nltk.cluster.util.cosine_distance,avoid_empty_clusters=True, repeats=25)
assigned_clusters = kclusterer.cluster(X, assign_clusters=True)
print 'Assigned into',NUM_CLUSTERS,' clusters:',assigned_clusters
print len(assigned_clusters)

#words = list(model.wv.vocab)
#print words
#for i, word in enumerate(words):  
#    print "Cluster ", str(assigned_clusters[i]), ":", word
#   
  
for index, sentence in enumerate(sentences):    
    print (str(assigned_clusters[index]) + ":" + str(sentence))
     
     
kmeans = cluster.KMeans(n_clusters=NUM_CLUSTERS)
kmeans.fit(X)
  
labels = kmeans.labels_
centroids = kmeans.cluster_centers_
  
print ("Cluster id labels for inputted data")
print (labels)
print ("Centroids data")
print (centroids)
  
print ("Score (Opposite of the value of X on the K-means objective which is Sum of distances of samples to their closest cluster center):")
print (kmeans.score(X))
  
silhouette_score = metrics.silhouette_score(X, labels, metric='euclidean')
  
print ("Silhouette_score: ")
print (silhouette_score)
 
 
import matplotlib.pyplot as plt
 
from sklearn.manifold import TSNE
 
model = TSNE(n_components=2, random_state=0)
np.set_printoptions(suppress=True)
 
Y=model.fit_transform(X)
 
 
plt.scatter(Y[:, 0], Y[:, 1], c=assigned_clusters, s=290,alpha=.5)
 
 
for j in range(len(sentences)):    
   plt.annotate(assigned_clusters[j],xy=(Y[j][0], Y[j][1]),xytext=(0,0),textcoords='offset points')
   print ("%s %s" % (assigned_clusters[j],  sentences[j]))
 
 
plt.show()