import nltk
import gensim
import numpy as np 
import matplotlib

from gensim.models import Word2Vec
from nltk.cluster import KMeansClusterer  
from sklearn import cluster
from sklearn import metrics
from sklearn.decomposition import PCA
matplotlib.use('Agg')
from matplotlib import pyplot

  
def sent_vectorizer(sent, model):
    sent_vec =[]
    numw = 0
    for w in sent:
        try:
            if numw == 0:
                sent_vec = model[w]
            else:
                sent_vec = np.add(sent_vec, model[w])
            numw+=1
        except:
            pass
     
    return np.asarray(sent_vec) / numw
  
  
# training data
  
#sentences = [['winter','is','coming'],
#			 ['tomorrow','will','snow'],
#             ['weather', 'rain', 'snow'],
#             ['the','weather','say','that','yesterday','it','will','snow'],
#             ['forecast', 'tomorrow', 'rain', 'snow'],
#			 ['there','are','many','adverts','on','TV'],
#			 ['what','else','can','go','wrong','in','this','drama','chapter'],
#			 ['yesterday','I','saw','a','disney','movie','it','was','so','sad'],
#			 ['I','found','this','book','in','a','blogger','post'],
#			 ['some','movies','are','awful'],
#			 ['I','hate','when','the','good','ones','die'],
#			 ['This','novel','is','fantastic'],
#			 ['I','love','watching','horror','films'],
#			 ['Today','is','gonna','be','a','good','day']]

sentences = [["I","found","a","love","for","me"],
			["darling","just","dive","right","in"],
			["and","follow","my","lead"],
			["well","I","found","a","girl","beautiful","and","sweet"],
			["I","never","knew","you","were","the","someone","waiting","for","me"],
			["cause","we","were","just","kids","when","we","fell","in","love"],
			["not","knowing","what","it","was"],
			["I","will","not","give","you","up","this","time"],
			["but","darling,","just","kiss","me","slow","your","heart","is","all","I","own"],
			["and","in","your","eyes","you","are","holding","mine"],
			["baby,","I","am","dancing","in","the","dark","with","you","between","my","arms"],
			["barefoot","on","the","grass,","listening","to","our","favorite","song"],
			["when","you","said","you","looked","a","mess","I","whispered","underneath","my","breath"],
			["but","you","heard","it","darling","you","look","perfect","tonight"],
			["well","I","found","a","woman","stronger","than","anyone","I","know"],
			["she","shares","my","dreams,","I","hope","that","someday","I","will","share","her","home"],
			["I","found","a","love,","to","carry","more","than","just","my","secrets"],
			["to","carry","love,","to","carry","children","of","our","own"],
			["we","are","still","kids","but","we're","so","in","love"],
			["fighting","against","all","odds"],
			["I","know","we","will","be","alright","this","time"],
			["darling,","just","hold","my","hand"],
			["be","my","girl,","I","will","be","your","man"],
			["I","see","my","future","in","your","eyes"],
			["baby,","I","am","dancing","in","the","dark,","with","you","between","my","arms"],
			["barefoot","on","the","grass,","listening","to","our","favorite","song"],
			["when","I","saw","you","in","that","dress,","looking","so","beautiful"],
			["I","don","not","deserve","this","darling,","you","look","perfect","tonight"],
			["baby,","I","am","dancing","in","the","dark,","with","you","between","my","arms"],
			["barefoot","on","the","grass,","listening","to","our","favorite","song"],
			["I","have","faith","in","what","I","see"],
			["now","I","know","I","have","met","an","angel","in","person"],
			["and","she","looks","perfect"],
			["I","do","not","deserve","this"],
			["you","look","perfect","tonight"]]
			 
model = Word2Vec(sentences, min_count=1)

print 'Model similiarity', model.similarity('love','perfect')
print 'Model similiarity', model.similarity('dancing','you')

  
X=[]
for sentence in sentences:
    X.append(sent_vectorizer(sentence, model))   

print ("========================")
print 'X', X
 
print ("========================")
print 'M1', model[model.wv.vocab]
 
print ("========================")
print 'M2', model.similarity('tonight', 'love')

print ("========================")
print 'M3', model.most_similar(positive=['perfect'], negative=[], topn=5)
print 'M3', model.most_similar(positive=[], negative=['perfect'], topn=5)

print ("========================")
print 'M4', model['the']
 
print "========================"
print 'M5', list(model.wv.vocab)

print "========================"
print 'M6', len(list(model.wv.vocab))
  
print "X shape", np.shape(X)  


# fit a 2d PCA model to the vectors
X2 = model[model.wv.vocab]
pca = PCA(n_components=2)
result = pca.fit_transform(X2)

# create a scatter plot of the projection
pyplot.scatter(result[:, 0], result[:, 1])
words = list(model.wv.vocab)
for i, word in enumerate(words):
	pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
pyplot.title('PCA')
pyplot.savefig('PCA_Perfect.pdf')
pyplot.close()
	

NUM_CLUSTERS = 2
kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=nltk.cluster.util.cosine_distance,avoid_empty_clusters=True, repeats=25)
assigned_clusters = kclusterer.cluster(X, assign_clusters=True)
print 'Assigned into',NUM_CLUSTERS,' clusters:',assigned_clusters
print len(assigned_clusters)

#words = list(model.wv.vocab)
#print words
#for i, word in enumerate(words):  
#    print "Cluster ", str(assigned_clusters[i]), ":", word
#   
  
for index, sentence in enumerate(sentences):    
    print (str(assigned_clusters[index]) + ":" + str(sentence))
     
     
kmeans = cluster.KMeans(n_clusters=NUM_CLUSTERS)
kmeans.fit(X)
  
labels = kmeans.labels_
centroids = kmeans.cluster_centers_
  
print ("Cluster id labels for inputted data")
print (labels)
print ("Centroids data")
print (centroids)
  
print ("Score (Opposite of the value of X on the K-means objective which is Sum of distances of samples to their closest cluster center):")
print (kmeans.score(X))
  
silhouette_score = metrics.silhouette_score(X, labels, metric='euclidean')
  
print ("Silhouette_score: ")
print (silhouette_score)
 
 
import matplotlib.pyplot as plt
 
from sklearn.manifold import TSNE
 
model = TSNE(n_components=2, random_state=0)
np.set_printoptions(suppress=True)
 
Y=model.fit_transform(X)
 
 
plt.scatter(Y[:, 0], Y[:, 1], c=assigned_clusters, s=290,alpha=.5)
 
 
for j in range(len(sentences)):    
   plt.annotate(assigned_clusters[j],xy=(Y[j][0], Y[j][1]),xytext=(0,0),textcoords='offset points')
   print ("%s %s" % (assigned_clusters[j],  sentences[j]))
 
 
plt.show()